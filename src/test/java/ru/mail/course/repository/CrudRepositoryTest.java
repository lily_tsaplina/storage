package ru.mail.course.repository;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.opentable.db.postgres.embedded.ConnectionInfo;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.hamcrest.Matchers;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.mail.course.DBConnetionData;
import ru.mail.course.TestEntities;
import ru.mail.course.model.Nomenclature;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static ru.mail.course.TestEntities.nomenclature;

public final class CrudRepositoryTest {
    @SuppressWarnings("NullableProblems")
    @Inject
    @NotNull
    private CrudRepository<Nomenclature, Integer> repository;

    private int freeId = 20;

    @Rule
    @NotNull
    public final PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @Before
    public void setUp() {
        ConnectionInfo connectionInfo = db.getConnectionInfo();
        DBConnetionData connetionData = new DBConnetionData(
                "jdbc:postgresql://localhost:" + connectionInfo.getPort() + "/" + connectionInfo.getDbName(),
                "postgres",
                "postgres"
        );
        Injector injector = Guice.createInjector(new TestModule(connetionData));
        injector.injectMembers(this);
    }

    @Test
    public void when_Find_Expect_ReturnNomenclature() throws SQLException {
        Nomenclature nomenclature = nomenclature(freeId++);
        repository.create(nomenclature);
        Nomenclature actual = repository.find(nomenclature.getId());
        Assert.assertEquals(nomenclature, actual);
        repository.delete(nomenclature.getId());
    }

    @Test
    public void when_FindAll_Expect_ReturnsAllEntries() throws SQLException {
        List<Nomenclature> expectedNomenclatures = Arrays.asList(
                TestEntities.nomenclature(freeId++),
                TestEntities.nomenclature(freeId++),
                TestEntities.nomenclature(freeId++)
        );
        for (Nomenclature nomenclature : expectedNomenclatures) {
            repository.create(nomenclature);
        }
        List<Nomenclature> actualNomenclatures = repository.findAll();
        Assert.assertEquals(expectedNomenclatures.size(), actualNomenclatures.size());
        for (Nomenclature expectedNomenclature : expectedNomenclatures) {
            Assert.assertThat(actualNomenclatures, Matchers.hasItem(expectedNomenclature));
        }
        for (Nomenclature nomenclature : expectedNomenclatures) {
            repository.delete(nomenclature.getId());
        }
    }

    @Test
    public void when_Create_Expect_EqualEntryInDB() throws SQLException {
        Nomenclature nomenclature = nomenclature(freeId++);
        repository.create(nomenclature);
        Nomenclature actual = repository.find(nomenclature.getId());
        Assert.assertEquals(nomenclature, actual);
        repository.delete(nomenclature.getId());
    }

    @Test(expected = SQLException.class)
    public void when_CreateExisting_Expect_Exception() throws SQLException {
        Nomenclature nomenclature = nomenclature(freeId++);
        repository.create(nomenclature);
        try {
            repository.create(nomenclature);
        } finally {
            repository.delete(nomenclature.getId());
        }
    }

    @Test
    public void when_Edit_Expect_NewValuesInOnlyThatEntryInDB() throws SQLException {
        Nomenclature nomenclature1 = nomenclature(freeId++);
        Nomenclature nomenclature2 = nomenclature(freeId++);
        repository.create(nomenclature1);
        repository.create(nomenclature2);

        Nomenclature nomenclatureToEdit = repository.find(nomenclature1.getId());
        if (nomenclatureToEdit != null) {
            String newName = nomenclatureToEdit.getName() + "1";
            nomenclatureToEdit.setName(newName);
            repository.update(nomenclatureToEdit);

            Nomenclature editedNomenclature = repository.find(nomenclatureToEdit.getId());

            Assert.assertEquals(nomenclatureToEdit, editedNomenclature);
            Assert.assertThat(repository.findAll(), Matchers.hasItem(nomenclature2));
        }
        repository.delete(nomenclature1.getId());
        repository.delete(nomenclature2.getId());
    }

    @Test
    public void when_Delete_Expect_DeletedOnlyThatEntry() throws SQLException {
        Nomenclature nomenclature1 = nomenclature(freeId++);
        repository.create(nomenclature1);

        Nomenclature nomenclature2 = nomenclature(freeId++);
        repository.create(nomenclature2);

        repository.delete(nomenclature1.getId());

        Assert.assertNull(repository.find(nomenclature1.getId()));
        Assert.assertThat(repository.findAll(), Matchers.hasItems(nomenclature2));
        repository.delete(nomenclature2.getId());
    }
}

package ru.mail.course.repository;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mail.course.DBConnetionData;
import ru.mail.course.model.Nomenclature;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@NoArgsConstructor
public final class TestModule extends AbstractModule {
    @NotNull
    private Connection connection;
    @Nullable
    private DBConnetionData connectionData;

    {
        try {
            this.connection = connection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public TestModule(@NotNull DBConnetionData connectionData) {
        this.connectionData = connectionData;
    }

    @Override
    protected void configure() {
        bind(Connection.class).toInstance(connection);
        bind(new TypeLiteral<CrudRepository<Nomenclature, Integer>>(){})
                .toInstance(new JDBCCrudRepository<>(Nomenclature.class));
    }

    @NotNull
    private Connection connection() throws SQLException {
        if (connectionData != null) {
            return DriverManager.getConnection(
                    connectionData.getUrl(),
                    connectionData.getUser(),
                    connectionData.getPassword()
            );
        } else {
            return DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/storage",
                "postgres",
                "blossom"
            );
        }

    }
}

package ru.mail.course;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.model.Company;
import ru.mail.course.model.Nomenclature;

public final class TestEntities {
    @NotNull
    public static Company company(int n) {
        return new Company(n, "company" + n, "tin" + n, "" + n);
    }

    @NotNull
    public static Nomenclature nomenclature(int n) {
        return new Nomenclature(n, "nomenclature" + n, "" + n);
    }
}

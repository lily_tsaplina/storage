package ru.mail.course;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.opentable.db.postgres.embedded.ConnectionInfo;
import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import net.lamberto.junit.GuiceJUnitRunner;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.mail.course.model.Company;
import ru.mail.course.model.Nomenclature;
import ru.mail.course.repository.CrudRepository;
import ru.mail.course.repository.JDBCCrudRepository;
import ru.mail.course.repository.TestModule;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("NullableProblems")
public final class CompanyRepositoryTest {
    @Rule
    @NotNull
    public final PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(FlywayPreparer.forClasspathLocation("db/migration"));

    @Inject
    @NotNull
    private CompanyRepository companyRepository;

    @Inject
    @NotNull
    private CrudRepository<Nomenclature, Integer> nomRep;

    @Before
    public void setUp() {
        ConnectionInfo connectionInfo = db.getConnectionInfo();
        DBConnetionData connetionData = new DBConnetionData(
                "jdbc:postgresql://localhost:" + connectionInfo.getPort() + "/" + connectionInfo.getDbName(),
                "postgres",
                "postgres"
        );
        Injector injector = Guice.createInjector(new TestModule(connetionData));
        injector.injectMembers(this);
    }

    @Test
    public void getFirstTenCompaniesByProductsCount() throws SQLException {
        List<Company> resultCompanies = companyRepository.getFirstTenCompaniesByProductsCount();
        String actual = resultCompanies.stream()
                .map(Company::getId)
                .map(Object::toString)
                .collect(Collectors.joining(" "));
        String expected = "1 8 3 4 6 7";
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getByProductsCountMoreThan() throws SQLException {
        Map<Nomenclature, Integer> nomenclaturesToTreasures = new HashMap<>();
        nomenclaturesToTreasures.put(nomRep.find(1), 10);
        nomenclaturesToTreasures.put(nomRep.find(2), 2);
        nomenclaturesToTreasures.put(nomRep.find(3), 20);
        List<Company> companies = companyRepository.getByProductsCountMoreThan(nomenclaturesToTreasures);
        List<Integer> expectedIds = Arrays.asList(1, 3);
        List<Integer> actualIds = companies.stream()
                .map(Company::getId)
                .collect(Collectors.toList());
        Assert.assertEquals(expectedIds, actualIds);
    }
}
insert into company(id, name, tin, checking_account)
values(1, 'company1', 'tin1', '1');
insert into company(id, name, tin, checking_account)
values(2, 'company2', 'tin2', '2');
insert into company(id, name, tin, checking_account)
values(3, 'company3', 'tin3', '3');
insert into company(id, name, tin, checking_account)
values(4, 'company4', 'tin4', '4');
insert into company(id, name, tin, checking_account)
values(5, 'company5', 'tin5', '5');
insert into company(id, name, tin, checking_account)
values(6, 'company6', 'tin6', '6');
insert into company(id, name, tin, checking_account)
values(7, 'company7', 'tin7', '7');
insert into company(id, name, tin, checking_account)
values(8, 'company8', 'tin8', '8');
insert into company(id, name, tin, checking_account)
values(9, 'company9', 'tin9', '9');
insert into company(id, name, tin, checking_account)
values(10, 'company10', 'tin10', '10');
insert into company(id, name, tin, checking_account)
values(11, 'company11', 'tin11', '11');
insert into company(id, name, tin, checking_account)
values(12, 'company12', 'tin12', '12');

insert into nomenclature(id, name, code)
values(1, 'nomenclature1', '1');
insert into nomenclature(id, name, code)
values(2, 'nomenclature2', '2');
insert into nomenclature(id, name, code)
values(3, 'nomenclature3', '3');
insert into nomenclature(id, name, code)
values(4, 'nomenclature4', '4');
insert into nomenclature(id, name, code)
values(5, 'nomenclature5', '5');

insert into waybill(id, waybill_date, company_id)
values(1, '2019-01-01', 1);
insert into waybill(id, waybill_date, company_id)
values(2, '2019-01-04', 1);
insert into waybill(id, waybill_date, company_id)
values(3, '2019-01-04', 2);
insert into waybill(id, waybill_date, company_id)
values(4, '2019-01-10', 3);
insert into waybill(id, waybill_date, company_id)
values(5, '2019-01-11', 3);
insert into waybill(id, waybill_date, company_id)
values(6, '2019-01-12', 4);
insert into waybill(id, waybill_date, company_id)
values(7, '2019-01-10', 5);
insert into waybill(id, waybill_date, company_id)
values(8, '2019-01-15', 6);
insert into waybill(id, waybill_date, company_id)
values(9, '2019-01-16', 7);
insert into waybill(id, waybill_date, company_id)
values(10, '2019-01-17', 8);
insert into waybill(id, waybill_date, company_id)
values(11, '2019-01-18', 9);
insert into waybill(id, waybill_date, company_id)
values(12, '2019-01-19', 10);
insert into waybill(id, waybill_date, company_id)
values(13, '2019-01-20', 11);

insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(1, 5, 5, 1, 1);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(2, 10, 10, 2, 1);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(3, 10, 10, 1, 2);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(4, 5, 5, 3, 1);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(5, 5, 5, 4, 2);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(6, 5, 5, 5, 1);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(7, 5, 5, 2, 5);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(8, 10, 10, 5, 10);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(9, 20, 1, 5, 9);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(10, 20, 5, 3, 8);
insert into waybillposition(id, price, count, nomenclature_id, waybill_id)
values(11, 10, 5, 1, 6);
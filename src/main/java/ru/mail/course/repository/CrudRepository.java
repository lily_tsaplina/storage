package ru.mail.course.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

/**
 * Интерфейс выполнения CRUD операций.
 *
 * @param <T> тип сущности
 * @param <K> тип идентификатора
 */
public interface CrudRepository<T, K> {
    void create(@NotNull T entity) throws SQLException;

    void update(T entity) throws SQLException;

    @Nullable
    T find(@NotNull K id) throws SQLException;

    @NotNull List<T> findAll() throws SQLException;

    void delete(@NotNull K id) throws SQLException;

    void clear() throws SQLException;
}

package ru.mail.course.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Реализация {@link CrudRepository} средствами JDBC.
 *
 * @param <T> тип сущности
 * @param <K> тип идентиафикатора
 */
@SuppressWarnings({"NullableProblems", "FinalOrAbstractClass"})
@NoArgsConstructor
public class JDBCCrudRepository<T, K> implements CrudRepository<T, K> {
    @NotNull
    @Inject
    private Connection connection;

    @NotNull
    private Class<T> clazz;

    @NotNull
    private Field[] fields;

    @NotNull
    private Class[] fieldsClasses;

    @NotNull
    private Field idField;

    public JDBCCrudRepository(@NotNull Class<T> clazz) {
        this.clazz = clazz;
        this.fields = clazz.getDeclaredFields();
        this.fieldsClasses = getFieldsClasses(fields);
        this.idField = Objects.requireNonNull(
                Arrays.stream(fields)
                    .filter(field -> field.getAnnotation(Id.class) != null)
                    .findFirst()
                    .orElseThrow(() -> new NullPointerException("No field annotated with @Id"))
        );
    }

    @Override
    public void create(@NotNull T entity) throws SQLException {
        StringBuilder queryBuilder = new StringBuilder();
        Field[] fields = entity.getClass().getDeclaredFields();
        String questionsByComma = Arrays.stream(fields)
                .map(field -> "?")
                .collect(Collectors.joining(", "));

        queryBuilder.append("INSERT INTO ")
                .append(entity.getClass().getSimpleName())
                .append("(")
                .append(Arrays.stream(fields)
                        .peek(field -> field.setAccessible(true))
                        .map(this::columnName)
                        .collect(Collectors.joining(", ")))
                .append(") VALUES(")
                .append(questionsByComma)
                .append(");");

        try (PreparedStatement statement = connection.prepareStatement(queryBuilder.toString())) {
            for (int i = 0; i < fields.length; i++) {
                try {
                    fields[i].setAccessible(true);
                    Object value = fields[i].get(entity);
                    statement.setObject(i + 1, value);
                    fields[i].setAccessible(false);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            statement.executeUpdate();
        }
    }

    @Override
    public void update(@NotNull T entity) throws SQLException {
        StringBuilder queryBuilder = new StringBuilder();

        queryBuilder.append("UPDATE ")
                .append(entity.getClass().getSimpleName())
                .append(" SET ");
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            String columnName = columnName(fields[i]);
            if (!fields[i].equals(idField)) {
                queryBuilder.append(columnName)
                        .append(" = ?");
                if (i != fields.length - 1) {
                    queryBuilder.append(", ");
                }
            }
        }
        queryBuilder.append(" WHERE ")
                .append(columnName(idField))
                .append(" = ?;");

        try {
            idField.setAccessible(true);
            idField.setAccessible(false);

            try (PreparedStatement statement = connection.prepareStatement(queryBuilder.toString())) {
                for (int i = 0; i < fields.length; i++) {
                    try {
                        fields[i].setAccessible(true);
                        if (!fields[i].equals(idField)) {
                            Object value = fields[i].get(entity);
                            statement.setObject(i, value);
                        }
                        fields[i].setAccessible(false);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

                idField.setAccessible(true);
                Object id = idField.get(entity);
                idField.setAccessible(false);
                statement.setObject(fields.length, id);

                statement.executeUpdate();
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Nullable
    public T find(@NotNull K id) throws SQLException {
        ResultSet resultSet;
        Object[] resultValues;

        String query = "SELECT * FROM "
                + clazz.getSimpleName()
                + " WHERE "
                + columnName(idField)
                + " = ?;";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setObject(1, id);
            resultSet = statement.executeQuery();

            resultValues = new Object[fields.length];
            if (resultSet.next()) {
                for (int i = 0; i < fields.length; i++) {
                    resultValues[i] = resultSet.getObject(columnName(fields[i]));
                }
                try {
                    Constructor<T> constructor = clazz.getConstructor(fieldsClasses);
                    return constructor.newInstance(resultValues);
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    @NotNull
    public List<T> findAll() throws SQLException {
        List<T> resultList = new ArrayList<>();
        String query = "SELECT * FROM " + clazz.getSimpleName();
        ResultSet resultSet;
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            resultSet = statement.executeQuery();
            Object[] resultValues;
            while(resultSet.next()) {
                resultValues = new Object[fields.length];
                for (int i = 0; i < fields.length; i++) {
                    resultValues[i] = resultSet.getObject(columnName(fields[i]));
                }
                try {
                    Constructor<T> constructor = clazz.getConstructor(fieldsClasses);
                    resultList.add(constructor.newInstance(resultValues));
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return resultList;
    }

    @Override
    public void delete(@NotNull K id) throws SQLException {
        String query = "DELETE FROM "
                + clazz.getSimpleName()
                + " WHERE "
                + columnName(idField)
                + " = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setObject(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void clear() throws SQLException {
        String query = "DELETE FROM "
                + clazz.getSimpleName();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.executeUpdate();
        }
    }

    @Nullable
    private String columnName(@NotNull Field field) {
        if (field.isAnnotationPresent(Column.class)) {
            Column column = field.getAnnotation(Column.class);
            return column.columnName();
        }
        return field.getName();
    }

    @NotNull
    private Class[] getFieldsClasses(@NotNull Field[] fields) {
        Class[] fieldsClasses = new Class[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            fieldsClasses[i] = fields[i].getType();
            fields[i].setAccessible(false);
        }
        return fieldsClasses;
    }
}

package ru.mail.course.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Представляет собой строку отчета по количествам и стоимости товаров за день или итоговые.
 */
@AllArgsConstructor
@Data
public final class CountPriceSum {
    @Nullable
    private Timestamp date; // null for total sums
    private int count;
    //@NotNull
    private BigDecimal cost;
}

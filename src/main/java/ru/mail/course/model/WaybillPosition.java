package ru.mail.course.model;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.repository.Column;
import ru.mail.course.repository.Id;

import java.math.BigDecimal;

/**
 * Позиция накладной.
 */
@Data
public final class WaybillPosition {
    @Id
    private int id;

    @SuppressWarnings("NullableProblems")
    @NotNull
    private BigDecimal price;

    private int count;

    @Column(columnName = "NOMENCLATURE_ID")
    private int nomenclatureId;

    @Column(columnName = "WAYBILL_ID")
    private int waybillId;
}

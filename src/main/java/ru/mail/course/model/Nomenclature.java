package ru.mail.course.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.repository.Id;

/**
 * Номенклатура.
 */
@Data
@AllArgsConstructor
public final class Nomenclature {
    @Id
    private int id;
    @NotNull
    private String name;
    @NotNull
    private String code;
}

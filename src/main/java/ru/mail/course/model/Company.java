package ru.mail.course.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.repository.Column;
import ru.mail.course.repository.Id;

/**
 * Организация (поставщик).
 */
@SuppressWarnings("NullableProblems")
@Data
@AllArgsConstructor
public final class Company {
    @Id
    private int id;

    @NotNull
    private String name;

    @NotNull
    private String tin;

    @Column(columnName = "CHECKING_ACCOUNT")
    @NotNull
    private String checkingAccount;
}

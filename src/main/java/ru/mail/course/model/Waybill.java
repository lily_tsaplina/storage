package ru.mail.course.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.repository.Column;
import ru.mail.course.repository.Id;

import java.sql.Timestamp;

/**
 * Накладная.
 */
@Data
@AllArgsConstructor
public final class Waybill {
    @Id
    private int id;

    @SuppressWarnings("NullableProblems")
    @NotNull
    @Column(columnName = "WAYBILL_DATE")
    private Timestamp date;

    @Column(columnName = "COMPANY_ID")
    private int companyId;
}

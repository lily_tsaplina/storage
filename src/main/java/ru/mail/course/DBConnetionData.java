package ru.mail.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
@Getter
public final class DBConnetionData {
    @NotNull
    private String url;
    @NotNull
    private String user;
    @NotNull
    private String password;
}
